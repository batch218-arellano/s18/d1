// console.log("Hello World");

function printInfo(){
	let nickname = prompt("Enter your nickname: ");
	console.log("Hi, " + nickname);
}

printInfo(); //invoke

				// 'name' - parameter
function printName(firstName){
	console.log("My name is " + firstName);
}

printName("Juana"); // 'Juana' - argument
printName("John");
printName("Cena");

// console.log(firstName); - will cause an error. 

// Now we have a reusable function / reusable task but could have a different output based on what value to process, with the help off parameters and arguments. 

// [Section] Parameters and Arguments

// Parameter
	// "firstName" is called a parameter
	//  A "parameter" acts as a named variable/container that exists only inside a function. 
	// It is used to store information that is provided to a function when it is called/ invoked. 

//Argument 
	// "Juana", "John", and "Cena" the information/data provided directly into the function is called "argument".
	// Values passed when invoking a function are called arguments.
	// These arguments are then stored as the parameter within the function.

let sampleVariable = "Inday";

printName(sampleVariable);
// Variables can also be passed as an argument

// ---------------------


function checkDivisibilityBy8(num){
	let remainder = num % 8;
	console.log("The remainder of " + num + " divided by 8 is: " + remainder);
	let isDivisibilityBy8 = remainder === 0; // will store a value true / false
	console.log("Is " + num + " divisible by 8?");
	console.log(isDivisibilityBy8);
}

checkDivisibilityBy8(64);
checkDivisibilityBy8(28);

// [Section] Function as argument
	// Function parameters can also accept functions as arguments
	// Some complex functions uses other functions to perform more complicated results.

	function argumentFunction(){
		console.log("This function was passed as an argument before the message was printed.");
	}

	function invokeFunction(argumentFunction){
		argumentFunction ();
	}

	invokeFunction(argumentFunction);
	console.log(argumentFunction);

	// ---------------------------


	// [Section] Using Multiple Parameters

		function createFullName(firstName, middleName, lastName){
			console.log("My full name is " + firstName + " " + middleName + " " + lastName);
		}
					//firstName		//middleName //lastName
		createFullName("Christopher", "Katigbak", "Malinao");
		createFullName("Jaye", "C...", "Arellano");

		// Using variables as an argument
		let firstName = "John";
		let middleName = "Doe";
		let lastName = "Smith";

		createFullName(firstName, middleName, lastName);

			function getDifferenceOf8Minus4(numA, numB){
				console.log("Difference: " + (numA - numB));
			}

		getDifferenceOf8Minus4(8,4);
		// getDifferenceOf8Minus4(4,8); // negative value, this will result to a 'logical error'.

		// [Section] Return statement

			// The "return" statement allows us to output a value from a function to be passed to the line/block of code that was invoked/called.

			function returnFullName(firstName, middleName, lastName){

				// This line of code will not be printed.
				console.log("This is printed inside a function");

				return firstName + " " + middleName + " " + lastName;

				
			}

			let completeName = returnFullName("Paul", "Smith", "Jordan")
			console.log(completeName);

			console.log("I am " + completeName);

			function printPLayerInfo(userName, level, job){
				console.log("Username: " + userName);
				console.log("Level: " + level); 
				console.log("Job: " + job); 
			}

			let user1 = printPLayerInfo("boxzMapagmahal", "Senior", "Programmer");
			console.log(user1); // returns undefined 





